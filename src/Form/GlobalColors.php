<?php
/**
 * @file
 * Contains \Drupal\d8learning\Form\GlobalColors.
 */
namespace Drupal\d8learning\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class GlobalColors extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'globalcolors_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('globalcolors_config.settings');
    $form['colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Colors'),
      '#description' => $this->t('Enter value in key|value format'),
      '#default_value' => $config->get('globalcolors_config.colors'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('globalcolors_config.settings');
    $config->set('globalcolors_config.colors', $form_state->getValue('colors'));
    $config->save();    

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'globalcolors_config.settings',
    ];
  }
}
